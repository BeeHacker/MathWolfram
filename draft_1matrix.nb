(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3036,         98]
NotebookOptionsPosition[      2721,         83]
NotebookOutlinePosition[      3063,         98]
CellTagsIndexPosition[      3020,         95]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"a11", "=", 
   RowBox[{"5", "+", 
    RowBox[{"6", "I"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"a12", "=", 
   RowBox[{"5", "+", 
    RowBox[{"7", "I"}]}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"a13", "=", 
   RowBox[{"78", "I"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"a21", "=", "67"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"a22", "=", "85"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"a23", "=", "4"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"a31", "=", "55"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"a32", "=", "67"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"a33", "=", "5"}], ";"}]}], "Input",
 CellChangeTimes->{{3.6991725383085957`*^9, 3.6991726824086275`*^9}, {
  3.699172721519292*^9, 3.699172737285697*^9}, {3.6991731919900613`*^9, 
  3.699173216451725*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"b11", "=", 
   RowBox[{"Conjugate", "[", "a11", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b12", "=", 
   RowBox[{"Conjugate", "[", "a12", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b13", "=", 
   RowBox[{"Conjugate", "[", "a13", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b21", "=", 
   RowBox[{"Conjugate", "[", "a21", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b22", "=", 
   RowBox[{"Conjugate", "[", "a22", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b23", "=", 
   RowBox[{"Conjugate", "[", "a23", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b31", "=", 
   RowBox[{"Conjugate", "[", "a31", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b32", "=", 
   RowBox[{"Conjugate", "[", "a32", "]"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"b33", "=", 
   RowBox[{"Conjugate", "[", "a33", "]"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.6991732799258747`*^9, 3.699173297056212*^9}, {
  3.69917335801105*^9, 3.6991734607243476`*^9}, {3.69917349285999*^9, 
  3.6991735452427588`*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{3.699173270659392*^9, 3.699173301133221*^9}]
},
WindowSize->{944, 709},
WindowMargins->{{25, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.4 for Microsoft Windows (64-bit) (April 11, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 913, 26, 246, "Input"],
Cell[1474, 48, 1150, 30, 409, "Input"],
Cell[2627, 80, 90, 1, 31, "Input"]
}
]
*)

(* End of internal cache information *)

