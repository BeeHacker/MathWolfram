(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3396,        119]
NotebookOptionsPosition[      2791,         93]
NotebookOutlinePosition[      3135,        108]
CellTagsIndexPosition[      3092,        105]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"solve", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"x", "^", "2"}], "-", 
     RowBox[{"x", "^", "4"}], "+", 
     RowBox[{"y", "^", "3"}]}], "=", "0"}], "]"}], 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.7305983973850713`*^9, 3.7305984387232122`*^9}}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"Set", "::", "write"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Tag \[NoBreak]\\!\\(\\*RowBox[{\\\"Plus\\\"}]\\)\[NoBreak] \
in \[NoBreak]\\!\\(\\*RowBox[{SuperscriptBox[\\\"x\\\", \\\"2\\\"], \
\\\"-\\\", SuperscriptBox[\\\"x\\\", \\\"4\\\"], \\\"+\\\", SuperscriptBox[\\\
\"y\\\", \\\"3\\\"]}]\\)\[NoBreak] is Protected. \\!\\(\\*ButtonBox[\\\"\
\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/General/write\\\", ButtonNote -> \
\\\"Set::write\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{3.730598443472458*^9}],

Cell[BoxData[
 RowBox[{"solve", "[", "0", "]"}]], "Output",
 CellChangeTimes->{3.730598443472458*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"solve", "[", 
   RowBox[{
    RowBox[{"x", "^", "3"}], "=", "91"}], "]"}], 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.7305985578586826`*^9, 3.7305985987023373`*^9}}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"Set", "::", "write"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Tag \
\[NoBreak]\\!\\(\\*RowBox[{\\\"Power\\\"}]\\)\[NoBreak] in \
\[NoBreak]\\!\\(\\*SuperscriptBox[\\\"x\\\", \\\"3\\\"]\\)\[NoBreak] is \
Protected. \\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", ButtonStyle->\\\"Link\
\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/General/write\\\", ButtonNote -> \
\\\"Set::write\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{3.730598601567906*^9}],

Cell[BoxData[
 RowBox[{"solve", "[", "91", "]"}]], "Output",
 CellChangeTimes->{3.730598601568907*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{"solve", "[", "91", "]"}], "]"}]], "Input",
 NumberMarks->False],

Cell[BoxData[
 RowBox[{"solve", "[", "91", "]"}]], "Output",
 CellChangeTimes->{3.7305986134200697`*^9}]
}, Open  ]]
},
WindowSize->{759, 601},
WindowMargins->{{Automatic, 295}, {34, Automatic}},
FrontEndVersion->"10.4 for Microsoft Windows (64-bit) (April 11, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 304, 9, 52, "Input"],
Cell[887, 33, 633, 12, 24, "Message"],
Cell[1523, 47, 101, 2, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1661, 54, 217, 6, 52, "Input"],
Cell[1881, 62, 526, 12, 24, "Message"],
Cell[2410, 76, 102, 2, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2549, 83, 119, 3, 48, "Input"],
Cell[2671, 88, 104, 2, 64, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

