(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.4' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      8315,        296]
NotebookOptionsPosition[      6970,        244]
NotebookOutlinePosition[      7355,        260]
CellTagsIndexPosition[      7312,        257]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{"f", ":=", "44"}]], "Input",
 CellChangeTimes->{{3.6964366137615585`*^9, 3.6964366281414213`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"M1", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"3", ",", " ", "4", ",", " ", "5"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"4", ",", " ", "5", ",", " ", "7"}], "}"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"5", ",", " ", "6", ",", " ", "7"}], "}"}]}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{"M2", " ", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"3", ",", " ", "4", ",", "5"}], "}"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"5", ",", " ", "5", ",", " ", "5"}], "}"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"1", ",", "5", ",", " ", "6"}], "}"}]}], 
   "}"}]}], "\[IndentingNewLine]", 
 RowBox[{"M3", "=", 
  RowBox[{"M1", "*", "M2"}]}]}], "Input",
 CellChangeTimes->{{3.6970306118227725`*^9, 3.697030757241696*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"3", ",", "4", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"4", ",", "5", ",", "7"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "6", ",", "7"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.6970307587332864`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"3", ",", "4", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "5", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "5", ",", "6"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.6970307587332864`*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"9", ",", "16", ",", "25"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"20", ",", "25", ",", "35"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "30", ",", "42"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.6970307587332864`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["M3"], "Input",
 CellChangeTimes->{{3.697030773629347*^9, 3.697030775245905*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"9", ",", "16", ",", "25"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"20", ",", "25", ",", "35"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "30", ",", "42"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.697030776190597*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ArrayPlot", "[", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"9", ",", "16", ",", "25"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"20", ",", "25", ",", "35"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"5", ",", "30", ",", "42"}], "}"}]}], "}"}], "]"}]], "Input",
 NumberMarks->False],

Cell[BoxData[
 GraphicsBox[
  RasterBox[{{37, 12, 0}, {22, 17, 7}, {33, 26, 17}}, {{0, 0}, {3, 3}}, {0, 
   42}],
  Frame->Automatic,
  FrameLabel->{None, None},
  FrameTicks->{{None, None}, {None, None}},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultPlotStyle" -> 
    Automatic}]], "Output",
 CellChangeTimes->{3.697030909034542*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ArrayPlot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"9", ",", "16", ",", "25"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"20", ",", "25", ",", "35"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"5", ",", "30", ",", "42"}], "}"}]}], "}"}], ",", 
   RowBox[{"PlotTheme", "\[Rule]", "\"\<Scientific\>\""}]}], "]"}]], "Input",
 NumberMarks->False],

Cell[BoxData[
 GraphicsBox[
  RasterBox[{{{0.972554, 0.79695, 0.5}, {0.6929344324324325, 
   0.3952742162162162, 0.30953227027027025`}, {0.292198, 0.342242, 
   0.606638}}, {{0.9218345405405406, 0.4945581081081081, 
   0.21508108108108104`}, {0.859907945945946, 0.417370972972973, 
   0.18573821621621628`}, {0.5259609189189188, 0.37317745945945946`, 
   0.4333263243243244}}, {{0.9590288108108108, 0.7163121621621622, 
   0.4240216216216216}, {0.9353597297297298, 0.5751959459459459, 
   0.29105945945945944`}, {0.859907945945946, 0.417370972972973, 
   0.18573821621621628`}}}, {{0, 0}, {3, 3}}, {0, 1}],
  Frame->Automatic,
  FrameLabel->{None, None},
  FrameTicks->{{None, None}, {None, None}},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  LabelStyle->{FontFamily -> "Times"},
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultPlotStyle" -> 
    Automatic}]], "Output",
 CellChangeTimes->{3.697030928621028*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"d1", "=", " ", 
  RowBox[{"Transpose", "[", "M1", "]"}]}]], "Input",
 CellChangeTimes->{{3.697031075959627*^9, 3.697031090512763*^9}, {
  3.6970312618485575`*^9, 3.697031293995552*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"3", ",", "4", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"4", ",", "5", ",", "6"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "7", ",", "7"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.6970312957837667`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Det", "[", "M1", "]"}]], "Input",
 CellChangeTimes->{{3.697031315689887*^9, 3.6970313295927143`*^9}}],

Cell[BoxData["2"], "Output",
 CellChangeTimes->{3.697031331390091*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Log", "[", "5", "]"}], "+", 
  RowBox[{"2", "/", "3"}], "-", 
  RowBox[{"Cos", "[", "1.2", "]"}]}]], "Input",
 CellChangeTimes->{{3.6970313946579056`*^9, 3.6970314386725082`*^9}}],

Cell[BoxData["1.9137468246240932`"], "Output",
 CellChangeTimes->{3.697031440429635*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Rationalize", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"Log", "[", "5", "]"}], "+", 
    RowBox[{"2", "/", "3"}], "-", 
    RowBox[{"Cos", "[", "1.2", "]"}]}], ",", 
   RowBox[{"10", "^", 
    RowBox[{"-", "20"}]}]}], "]"}]], "Input",
 CellChangeTimes->{{3.697031454502236*^9, 3.697031638773247*^9}}],

Cell[BoxData[
 FractionBox["178590203", "93319660"]], "Output",
 CellChangeTimes->{3.6970316411872845`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Sqrt", "[", 
  RowBox[{"-", "13"}], "]"}]], "Input",
 CellChangeTimes->{{3.6978197464686966`*^9, 3.6978197484240446`*^9}}],

Cell[BoxData[
 RowBox[{"\[ImaginaryI]", " ", 
  SqrtBox["13"]}]], "Output",
 CellChangeTimes->{3.6978197499399786`*^9}]
}, Open  ]]
},
WindowSize->{681, 686},
WindowMargins->{{-7, Automatic}, {Automatic, 0}},
PrintingPageRange->{Automatic, Automatic},
FrontEndVersion->"10.4 for Microsoft Windows (64-bit) (April 11, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 121, 2, 31, "Input"],
Cell[CellGroupData[{
Cell[704, 26, 805, 23, 72, "Input"],
Cell[1512, 51, 304, 9, 31, "Output"],
Cell[1819, 62, 304, 9, 31, "Output"],
Cell[2126, 73, 311, 9, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2474, 87, 94, 1, 31, "Input"],
Cell[2571, 90, 309, 9, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2917, 104, 330, 10, 48, "Input"],
Cell[3250, 116, 405, 12, 374, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3692, 133, 416, 12, 86, "Input"],
Cell[4111, 147, 937, 20, 374, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5085, 172, 209, 4, 48, "Input"],
Cell[5297, 178, 304, 9, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5638, 192, 125, 2, 48, "Input"],
Cell[5766, 196, 70, 1, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5873, 202, 214, 5, 48, "Input"],
Cell[6090, 209, 88, 1, 31, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6215, 215, 324, 9, 48, "Input"],
Cell[6542, 226, 107, 2, 46, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6686, 233, 146, 3, 48, "Input"],
Cell[6835, 238, 119, 3, 34, "Output"]
}, Open  ]]
}
]
*)

