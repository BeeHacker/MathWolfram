(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 9.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     35617,       1512]
NotebookOptionsPosition[     33321,       1429]
NotebookOutlinePosition[     33675,       1445]
CellTagsIndexPosition[     33632,       1442]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Cycloid Catacaustic", "Title"],

Cell[CellGroupData[{

Cell["Author", "Subsection"],

Cell["\<\
Eric W. Weisstein
March 16, 2008\
\>", "Text"],

Cell[TextData[{
 "This notebook downloaded from ",
 ButtonBox["http://mathworld.wolfram.com/notebooks/PlaneCurves/\
CycloidCatacaustic.nb",
  BaseStyle->"Hyperlink",
  ButtonData:>{
    URL["http://mathworld.wolfram.com/notebooks/PlaneCurves/\
CycloidCatacaustic.nb"], None}],
 "."
}], "Text"],

Cell[TextData[{
 "For more information, see Eric's ",
 StyleBox["MathWorld",
  FontSlant->"Italic"],
 " entry ",
 ButtonBox["http://mathworld.wolfram.com/CycloidCatacaustic.html",
  BaseStyle->"Hyperlink",
  ButtonData:>{
    URL["http://mathworld.wolfram.com/CycloidCatacaustic.html"], None}],
 "."
}], "Text"],

Cell["\<\
\[Copyright]2008 Wolfram Research, Inc. except for portions noted otherwise\
\>", "Text"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Initialization", "Subsection"],

Cell[BoxData[
 RowBox[{"<<", "MathWorld`Curves`"}]], "Input",
 InitializationCell->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["Equation", "Section"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"cat", "=", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{"Catacaustic", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"t", "-", 
        RowBox[{"Sin", "[", "t", "]"}]}], ",", 
       RowBox[{"1", "-", 
        RowBox[{"Cos", "[", "t", "]"}]}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "y"}], "}"}], ",", "t"}], "]"}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "x"}], "+", 
         RowBox[{"t", " ", 
          RowBox[{"(", 
           RowBox[{"1", "+", 
            SuperscriptBox[
             RowBox[{"(", 
              RowBox[{"t", "-", "x"}], ")"}], "2"], "+", 
            RowBox[{
             RowBox[{"(", 
              RowBox[{
               RowBox[{"-", "1"}], "+", "y"}], ")"}], " ", "y"}]}], ")"}]}]}],
         ")"}]}], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"-", "2"}], " ", "x", " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "2"}], "+", "y"}], ")"}]}], "+", 
         RowBox[{"4", " ", "t", " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "1"}], "+", "y"}], ")"}]}]}], ")"}], " ", 
       RowBox[{"Cos", "[", "t", "]"}]}], "-", 
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{"t", "-", "x"}], ")"}], " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", "y"}], ")"}], " ", 
       RowBox[{"Cos", "[", 
        RowBox[{"2", " ", "t"}], "]"}]}], "-", 
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "2"}], "+", 
         RowBox[{"2", " ", 
          SuperscriptBox["t", "2"]}], "-", 
         RowBox[{"3", " ", "t", " ", "x"}], "+", 
         SuperscriptBox["x", "2"], "+", 
         RowBox[{"2", " ", "y"}]}], ")"}], " ", 
       RowBox[{"Sin", "[", "t", "]"}]}], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "2"}], "+", 
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{"t", "-", "x"}], ")"}], "2"], "-", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "2"}], "+", "y"}], ")"}], " ", "y"}]}], ")"}], " ", 
       RowBox[{"Sin", "[", 
        RowBox[{"2", " ", "t"}], "]"}]}]}], ")"}], "/", 
    RowBox[{"(", 
     RowBox[{"2", " ", 
      RowBox[{"(", 
       RowBox[{
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"t", "-", "x"}], ")"}], "2"], "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "1"}], "+", "y"}], ")"}], " ", "y"}], "+", 
        RowBox[{"y", " ", 
         RowBox[{"Cos", "[", "t", "]"}]}], "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "t"}], "+", "x"}], ")"}], " ", 
         RowBox[{"Sin", "[", "t", "]"}]}]}], ")"}]}], ")"}]}], ",", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"2", " ", 
      RowBox[{"Sin", "[", 
       FractionBox["t", "2"], "]"}], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{"t", "-", "x"}], ")"}], " ", 
         RowBox[{"Cos", "[", 
          FractionBox["t", "2"], "]"}], " ", 
         RowBox[{"(", 
          RowBox[{"2", "-", "y", "+", 
           RowBox[{"2", " ", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{"-", "1"}], "+", "y"}], ")"}], " ", 
            RowBox[{"Cos", "[", "t", "]"}]}]}], ")"}]}], "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "2"}], "+", 
           SuperscriptBox["y", "2"], "-", 
           RowBox[{
            RowBox[{"(", 
             RowBox[{
              RowBox[{"-", "2"}], "+", 
              SuperscriptBox[
               RowBox[{"(", 
                RowBox[{"t", "-", "x"}], ")"}], "2"], "-", 
              RowBox[{
               RowBox[{"(", 
                RowBox[{
                 RowBox[{"-", "2"}], "+", "y"}], ")"}], " ", "y"}]}], ")"}], 
            " ", 
            RowBox[{"Cos", "[", "t", "]"}]}]}], ")"}], " ", 
         RowBox[{"Sin", "[", 
          FractionBox["t", "2"], "]"}]}]}], ")"}]}], ")"}], "/", 
    RowBox[{"(", 
     RowBox[{
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"t", "-", "x"}], ")"}], "2"], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", "y"}], ")"}], " ", "y"}], "+", 
      RowBox[{"y", " ", 
       RowBox[{"Cos", "[", "t", "]"}]}], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "t"}], "+", "x"}], ")"}], " ", 
       RowBox[{"Sin", "[", "t", "]"}]}]}], ")"}]}]}], "}"}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell["At origin", "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FullSimplify", "[", 
  RowBox[{"cat", "/.", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"x", "\[Rule]", "0"}], ",", 
     RowBox[{"y", "\[Rule]", "0"}]}], "}"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    FractionBox["1", 
     RowBox[{"t", " ", 
      RowBox[{"(", 
       RowBox[{"t", "-", 
        RowBox[{"Sin", "[", "t", "]"}]}], ")"}]}]], 
    RowBox[{"(", 
     RowBox[{"t", "+", 
      SuperscriptBox["t", "3"], "+", 
      RowBox[{"2", " ", 
       RowBox[{"Sin", "[", "t", "]"}]}], "+", 
      RowBox[{"t", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"-", "2"}], " ", 
          RowBox[{"Cos", "[", "t", "]"}]}], "+", 
         RowBox[{"Cos", "[", 
          RowBox[{"2", " ", "t"}], "]"}], "+", 
         RowBox[{"t", " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{"-", "2"}], "+", 
            RowBox[{"Cos", "[", "t", "]"}]}], ")"}], " ", 
          RowBox[{"Sin", "[", "t", "]"}]}]}], ")"}]}], "-", 
      RowBox[{"Sin", "[", 
       RowBox[{"2", " ", "t"}], "]"}]}], ")"}]}], ",", 
   RowBox[{"-", 
    FractionBox[
     RowBox[{"2", " ", 
      SuperscriptBox[
       RowBox[{"Sin", "[", 
        FractionBox["t", "2"], "]"}], "2"], " ", 
      RowBox[{"(", 
       RowBox[{"2", "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"-", "2"}], "+", 
           SuperscriptBox["t", "2"]}], ")"}], " ", 
         RowBox[{"Cos", "[", "t", "]"}]}], "-", 
        RowBox[{"2", " ", "t", " ", 
         RowBox[{"Sin", "[", "t", "]"}]}]}], ")"}]}], 
     RowBox[{"t", " ", 
      RowBox[{"(", 
       RowBox[{"t", "-", 
        RowBox[{"Sin", "[", "t", "]"}]}], ")"}]}]]}]}], "}"}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "At ",
 Cell[BoxData[
  FormBox[
   RowBox[{"x", "=", "\[Infinity]"}], TraditionalForm]]]
}], "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{"cat", ",", 
   RowBox[{"x", "\[Rule]", "\[Infinity]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"t", "+", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "1"}], "+", 
       RowBox[{"Cos", "[", "t", "]"}]}], ")"}], " ", 
     RowBox[{"Sin", "[", "t", "]"}]}]}], ",", 
   RowBox[{
    RowBox[{"-", "2"}], " ", 
    RowBox[{"Cos", "[", "t", "]"}], " ", 
    SuperscriptBox[
     RowBox[{"Sin", "[", 
      FractionBox["t", "2"], "]"}], "2"]}]}], "}"}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "At ",
 Cell[BoxData[
  FormBox[
   RowBox[{"y", "=", "\[Infinity]"}], TraditionalForm]]]
}], "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{"cat", ",", 
   RowBox[{"y", "\[Rule]", "\[Infinity]"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"t", "-", 
    RowBox[{
     RowBox[{"Cos", "[", "t", "]"}], " ", 
     RowBox[{"Sin", "[", "t", "]"}]}]}], ",", 
   SuperscriptBox[
    RowBox[{"Sin", "[", "t", "]"}], "2"]}], "}"}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"TrigReduce", "[", 
  RowBox[{"FullSimplify", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"t", "-", 
       RowBox[{
        RowBox[{"Cos", "[", "t", "]"}], " ", 
        RowBox[{"Sin", "[", "t", "]"}]}]}], ",", 
      SuperscriptBox[
       RowBox[{"Sin", "[", "t", "]"}], "2"]}], "}"}], "/.", 
    RowBox[{"t", "\[Rule]", 
     RowBox[{"t", "/", "2"}]}]}], "]"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{"t", "-", 
      RowBox[{"Sin", "[", "t", "]"}]}], ")"}]}], ",", 
   RowBox[{
    FractionBox["1", "2"], " ", 
    RowBox[{"(", 
     RowBox[{"1", "-", 
      RowBox[{"Cos", "[", "t", "]"}]}], ")"}]}]}], "}"}]], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Plot", "Section"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ParametricPlot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"t", "-", 
      RowBox[{"Sin", "[", "t", "]"}]}], ",", 
     RowBox[{"1", "-", 
      RowBox[{"Cos", "[", "t", "]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", 
     RowBox[{"2", "\[Pi]"}]}], "}"}], ",", 
   RowBox[{"AspectRatio", "\[Rule]", "Automatic"}]}], "]"}]], "Input"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: .31831 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.0238095 0.151576 0.00757881 0.151576 [
[.17539 -0.00492 -3 -9 ]
[.17539 -0.00492 3 0 ]
[.32696 -0.00492 -3 -9 ]
[.32696 -0.00492 3 0 ]
[.47854 -0.00492 -3 -9 ]
[.47854 -0.00492 3 0 ]
[.63011 -0.00492 -3 -9 ]
[.63011 -0.00492 3 0 ]
[.78169 -0.00492 -3 -9 ]
[.78169 -0.00492 3 0 ]
[.93327 -0.00492 -3 -9 ]
[.93327 -0.00492 3 0 ]
[.01131 .08337 -18 -4.5 ]
[.01131 .08337 0 4.5 ]
[.01131 .15915 -6 -4.5 ]
[.01131 .15915 0 4.5 ]
[.01131 .23494 -18 -4.5 ]
[.01131 .23494 0 4.5 ]
[.01131 .31073 -6 -4.5 ]
[.01131 .31073 0 4.5 ]
[ 0 0 0 0 ]
[ 1 .31831 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 g
.25 Mabswid
[ ] 0 setdash
.17539 .00758 m
.17539 .01383 L
s
[(1)] .17539 -0.00492 0 1 Mshowa
.32696 .00758 m
.32696 .01383 L
s
[(2)] .32696 -0.00492 0 1 Mshowa
.47854 .00758 m
.47854 .01383 L
s
[(3)] .47854 -0.00492 0 1 Mshowa
.63011 .00758 m
.63011 .01383 L
s
[(4)] .63011 -0.00492 0 1 Mshowa
.78169 .00758 m
.78169 .01383 L
s
[(5)] .78169 -0.00492 0 1 Mshowa
.93327 .00758 m
.93327 .01383 L
s
[(6)] .93327 -0.00492 0 1 Mshowa
.125 Mabswid
.05412 .00758 m
.05412 .01133 L
s
.08444 .00758 m
.08444 .01133 L
s
.11476 .00758 m
.11476 .01133 L
s
.14507 .00758 m
.14507 .01133 L
s
.2057 .00758 m
.2057 .01133 L
s
.23602 .00758 m
.23602 .01133 L
s
.26633 .00758 m
.26633 .01133 L
s
.29665 .00758 m
.29665 .01133 L
s
.35728 .00758 m
.35728 .01133 L
s
.38759 .00758 m
.38759 .01133 L
s
.41791 .00758 m
.41791 .01133 L
s
.44822 .00758 m
.44822 .01133 L
s
.50885 .00758 m
.50885 .01133 L
s
.53917 .00758 m
.53917 .01133 L
s
.56948 .00758 m
.56948 .01133 L
s
.5998 .00758 m
.5998 .01133 L
s
.66043 .00758 m
.66043 .01133 L
s
.69074 .00758 m
.69074 .01133 L
s
.72106 .00758 m
.72106 .01133 L
s
.75137 .00758 m
.75137 .01133 L
s
.81201 .00758 m
.81201 .01133 L
s
.84232 .00758 m
.84232 .01133 L
s
.87264 .00758 m
.87264 .01133 L
s
.90295 .00758 m
.90295 .01133 L
s
.96358 .00758 m
.96358 .01133 L
s
.9939 .00758 m
.9939 .01133 L
s
.25 Mabswid
0 .00758 m
1 .00758 L
s
.02381 .08337 m
.03006 .08337 L
s
[(0.5)] .01131 .08337 1 0 Mshowa
.02381 .15915 m
.03006 .15915 L
s
[(1)] .01131 .15915 1 0 Mshowa
.02381 .23494 m
.03006 .23494 L
s
[(1.5)] .01131 .23494 1 0 Mshowa
.02381 .31073 m
.03006 .31073 L
s
[(2)] .01131 .31073 1 0 Mshowa
.125 Mabswid
.02381 .02274 m
.02756 .02274 L
s
.02381 .03789 m
.02756 .03789 L
s
.02381 .05305 m
.02756 .05305 L
s
.02381 .06821 m
.02756 .06821 L
s
.02381 .09852 m
.02756 .09852 L
s
.02381 .11368 m
.02756 .11368 L
s
.02381 .12884 m
.02756 .12884 L
s
.02381 .144 m
.02756 .144 L
s
.02381 .17431 m
.02756 .17431 L
s
.02381 .18947 m
.02756 .18947 L
s
.02381 .20463 m
.02756 .20463 L
s
.02381 .21979 m
.02756 .21979 L
s
.02381 .2501 m
.02756 .2501 L
s
.02381 .26526 m
.02756 .26526 L
s
.02381 .28042 m
.02756 .28042 L
s
.02381 .29557 m
.02756 .29557 L
s
.25 Mabswid
.02381 0 m
.02381 .31831 L
s
0 0 m
1 0 L
1 .31831 L
0 .31831 L
closepath
clip
newpath
.5 Mabswid
.02381 .00758 m
.02381 .00758 L
.02381 .0076 L
.02381 .00762 L
.02381 .00765 L
.02381 .00773 L
.02381 .00784 L
.02382 .00801 L
.02383 .00823 L
.02386 .00874 L
.02391 .00942 L
.02397 .0102 L
.02423 .01248 L
.02467 .01551 L
.02528 .01889 L
.02758 .02859 L
.03086 .03924 L
.0361 .05299 L
.05248 .08545 L
.07719 .12171 L
.11412 .1629 L
.16042 .20236 L
.21452 .23752 L
.24522 .25356 L
.28118 .26934 L
.31777 .28246 L
.35256 .29243 L
.38977 .3006 L
.41067 .3041 L
.43066 .30675 L
.44978 .30865 L
.45947 .30937 L
.4702 .31 L
.47933 .31038 L
.48446 .31053 L
.48691 .31059 L
.48918 .31063 L
.49135 .31067 L
.49333 .31069 L
.49552 .31071 L
.49787 .31073 L
.50012 .31073 L
.50257 .31073 L
.50488 .31071 L
.50701 .31069 L
.50952 .31066 L
.5119 .31061 L
.51636 .31051 L
.52116 .31036 L
.52639 .31016 L
Mistroke
.53577 .30967 L
.54446 .3091 L
.56429 .30731 L
.58525 .3047 L
.60536 .30148 L
.62681 .29727 L
.66477 .28776 L
.70096 .2761 L
.73731 .2616 L
.79727 .23067 L
.85188 .19287 L
.89456 .15419 L
.92901 .1135 L
.95205 .07746 L
.96589 .04808 L
.97035 .03559 L
.97345 .02464 L
.97437 .02057 L
.97511 .01678 L
.97556 .01402 L
.97589 .01156 L
.97606 .0098 L
.97615 .00863 L
.97617 .00816 L
.97618 .00786 L
.97619 .00774 L
.97619 .00769 L
.97619 .00764 L
.97619 .00762 L
.97619 .00759 L
.97619 .00758 L
.97619 .00758 L
Mfstroke
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
 ImageSize->{288, 91.625},
 ImageMargins->{{43, 0}, {0, 0}},
 ImageRegion->{{0, 1}, {0, 
  1}},ImageCache->GraphicsData["CompressedBitmap", "\<\
eJztmMtLVUEcx8f7SLO6mqWp6b3Hm2KmWRiS9KAQfERlD3pABXUVSRdSmbSI
ok2L2rSoTRBtCiJoUUS0KIKIKKKoqKiIoAc9/oM2Lsz5zfF3z537nTn3aC2C
XJwz/Ob7+3x/M2fOnLluTI0M9A+lRgb7Uk7XcOrgwGDfYafzwPB4KJwnRJ4j
hNjtCNkeG2+6F/qrkRchOuQtLOMUiHKLYgsye6mV5FZCITrpRpEx90/JRcQb
k22H7tGMsNulvDRMwqK3seLmypIgVgticZ4OY29wczRGmqoQz3H2pNWZ/CM+
D8E2Q2hQCS0mJmqKpKtDDmoRhXhxTOKBJbVJ8J82y7jRWjWONzqWYe9iSVCT
3UarZ9S3q0refsmemFkbBbFIjrFcc/+07r/Hv+KRtauEgdSyOn+AWIlHTi/P
z1xEiDTHIBJTEpHHdx+Rtt8FlH8D8rleeda8fwUZpfrI1NOZSBVffHKyXVBG
mTcDb8OfJ5f2CaTN08eEFtxHc2IRJ6JFHZJXCqFXIxTMKobnMN8yYlQTGuAH
4FpudZ1hHe1f9shhO3kP3Cr85zFielbI4x3wqPT30CjTQAxV8Nbsll6EBbIV
5eUYMpm+AbD5Coa2VVTja4CoMiNQFa+CIQpA7CVAVHsQNAPTQeILkBg3eyPE
82CIQhB7BhCJYIinAOHoMzATJD4BiZbzI0I8BoikGTELxB75IKj8IpD4ECSq
E7h6LYzbLoI9ADB1FndfC//doBjE7gNsXTDsbBC7RxMjqQt9YBQqAb13QWV+
MHQaosdzB8AaFKySRWgPKQWx2wC2KFhlZSB2C2AbFTa9zspB4k2Q2ORJ1OQI
cQMgFpsRFVzPdZDYrBddBRDXQOISsyNCXCUDSWgxJ1aD2BXgbUHEQewye7fq
o3W4dQn4eOXwfz2ZsYvs02ZOTHtfYPkKs7yW5edBgd5EEtUDxDn2Wa3kdJwI
82c90Cmznq3OgnpcA7QnBj1YNrDRGa5/rRlvP0E2gt7TjG2fBFaLNXG1p8C0
WAzs58Rmxp7kajsULMZd6DCBtselIHaCsV0+NWrHwBZuHWfEOoVAy3gZy4+C
+bEktnLiEfbZYJYvl1cKHWJ5j0dOpJXcGmLRZjNzFTP3U0vKt5rla1jey/Tt
Znk7y/cwfadecSeLttGzkKJdZmY3y3uYudcsX8/ybpb36iVsYlE7i/qViL4w
6fWh0bdwbxvXPqAS1Q+OrN81iLJDXgtlqIEpQ0qf63d8n7wWyBD9BC6UiGGF
QF+sMA+Yqi2WrXyZc8wz6gAb2uiU0tKHUAorpWq7p2yR9xufxWoY\
\>"],ImageRangeCache->{{{0, 287}, {90.625, 0}} -> {0, 0, 0, 0}}],

Cell[BoxData[
 TagBox[
  RowBox[{"\[SkeletonIndicator]", "Graphics", "\[SkeletonIndicator]"}],
  False,
  Editable->False]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"CatacausticPlot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"t", "-", 
      RowBox[{"Sin", "[", "t", "]"}]}], ",", 
     RowBox[{"1", "-", 
      RowBox[{"Cos", "[", "t", "]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "100000"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"t", ",", "0", ",", 
     RowBox[{"2", "\[Pi]"}], ",", 
     RowBox[{"2", 
      RowBox[{"\[Pi]", "/", "30"}]}]}], "}"}]}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Power", "::", "\<\"infy\"\>"}], ":", 
  " ", "\<\"Infinite expression \\!\\(1\\/0\\) encountered. \
(\\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\", \
ButtonStyle->\\\"OtherInformationLink\\\", ButtonFrame->None, \
ButtonData:>\\\"Power::infy\\\"]\\))\"\>"}]], "Message"],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Infinity]", "::", "\<\"indet\"\>"}], ":", 
  " ", "\<\"Indeterminate expression \\!\\(0\\\\ \
\\*InterpretationBox[\\\"ComplexInfinity\\\", DirectedInfinity[]]\\) \
encountered. (\\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\", \
ButtonStyle->\\\"OtherInformationLink\\\", ButtonFrame->None, \
ButtonData:>\\\"function::indet\\\"]\\))\"\>"}]], "Message"],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Infinity]", "::", "\<\"indet\"\>"}], ":", 
  " ", "\<\"Indeterminate expression \\!\\(0\\\\ \
\\*InterpretationBox[\\\"ComplexInfinity\\\", DirectedInfinity[]]\\) \
encountered. (\\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\", \
ButtonStyle->\\\"OtherInformationLink\\\", ButtonFrame->None, \
ButtonData:>\\\"function::indet\\\"]\\))\"\>"}]], "Message"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Power", "::", "\<\"infy\"\>"}], ":", 
  " ", "\<\"Infinite expression \\!\\(1\\/0\\) encountered. \
(\\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\", \
ButtonStyle->\\\"OtherInformationLink\\\", ButtonFrame->None, \
ButtonData:>\\\"Power::infy\\\"]\\))\"\>"}]], "Message"],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Infinity]", "::", "\<\"indet\"\>"}], ":", 
  " ", "\<\"Indeterminate expression \\!\\(0\\\\ \
\\*InterpretationBox[\\\"ComplexInfinity\\\", DirectedInfinity[]]\\) \
encountered. (\\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\", \
ButtonStyle->\\\"OtherInformationLink\\\", ButtonFrame->None, \
ButtonData:>\\\"function::indet\\\"]\\))\"\>"}]], "Message"],

Cell[BoxData[
 RowBox[{
  RowBox[{"General", "::", "\<\"stop\"\>"}], ":", 
  " ", "\<\"Further output of \\!\\(\[Infinity] :: \\\"indet\\\"\\) will be \
suppressed during this calculation. (\\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\
\", ButtonStyle->\\\"OtherInformationLink\\\", ButtonFrame->None, \
ButtonData:>\\\"function::stop\\\"]\\))\"\>"}]], "Message"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Graphics", "::", "\<\"gptn\"\>"}], ":", 
  " ", "\<\"Coordinate \\!\\(Indeterminate\\) in \\!\\({Indeterminate, \
Indeterminate}\\) is not a floating-point number. \
(\\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\", \
ButtonStyle->\\\"OtherInformationLink\\\", ButtonFrame->None, \
ButtonData:>\\\"Graphics::gptn\\\"]\\))\"\>"}]], "Message"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Graphics", "::", "\<\"gptn\"\>"}], ":", 
  " ", "\<\"Coordinate \\!\\(Indeterminate\\) in \\!\\({Indeterminate, \
Indeterminate}\\) is not a floating-point number. \
(\\!\\(\\*ButtonBox[\\\"More\[Ellipsis]\\\", \
ButtonStyle->\\\"OtherInformationLink\\\", ButtonFrame->None, \
ButtonData:>\\\"Graphics::gptn\\\"]\\))\"\>"}]], "Message"],

Cell[GraphicsData["PostScript", "\<\
%!
%%Creator: Mathematica
%%AspectRatio: .32416 
MathPictureStart
/Mabs {
Mgmatrix idtransform
Mtmatrix dtransform
} bind def
/Mabsadd { Mabs
3 -1 roll add
3 1 roll add
exch } bind def
%% Graphics
%%IncludeResource: font Courier
%%IncludeFont: Courier
/Courier findfont 10  scalefont  setfont
% Scaling calculations
0.380952 0.0386458 0.123401 0.0386458 [
[ 0 0 0 0 ]
[ 1 .32416 0 0 ]
] MathScale
% Start of Graphics
1 setlinecap
1 setlinejoin
newpath
0 0 m
1 0 L
1 .32416 L
0 .32416 L
closepath
clip
newpath
1 0 0 r
.5 Mabswid
[ ] 0 setdash
.38095 .32416 m
.38095 .1234 L
s
.38101 .32416 m
.38101 .12425 L
s
.38101 .12425 m
.3546 0 L
s
.38142 .32416 m
.38142 .12674 L
s
.38142 .12674 m
.32499 0 L
s
.38252 .32416 m
.38252 .13078 L
s
.38252 .13078 m
.2875 0 L
s
.38461 .32416 m
.38461 .13619 L
s
.38461 .13619 m
.23336 0 L
s
.38795 .32416 m
.38795 .14272 L
s
.38795 .14272 m
.14075 0 L
s
.39276 .32416 m
.39276 .1501 L
s
.39276 .1501 m
0 .02249 L
s
.39917 .32416 m
.39918 .15801 L
s
.39918 .15801 m
0 .11605 L
s
.40727 .32416 m
.40727 .16609 L
s
.40727 .16609 m
0 .20889 L
s
.41704 .32416 m
.41704 .17399 L
s
.41704 .17399 m
0 .3095 L
s
.42842 .32416 m
.42842 .18137 L
s
.42842 .18137 m
.1811 .32416 L
s
.44126 .32416 m
.44127 .18791 L
s
.44127 .18791 m
.28994 .32416 L
s
.45536 .32416 m
.45536 .19331 L
s
.45536 .19331 m
.3603 .32416 L
s
.47045 .32416 m
.47045 .19735 L
s
.47045 .19735 m
.414 .32416 L
s
.48623 .32416 m
.48623 .19985 L
s
.48623 .19985 m
.45981 .32416 L
s
.50236 .32416 m
.50236 .20069 L
s
.50236 .20069 m
.50237 .32416 L
s
.51849 .32416 m
.51849 .19985 L
s
.51849 .19985 m
.54492 .32416 L
s
.53426 .32416 m
.53427 .19735 L
s
.53427 .19735 m
.59073 .32416 L
s
.54935 .32416 m
.54936 .19331 L
s
.54936 .19331 m
.64444 .32416 L
s
.56345 .32416 m
.56346 .18791 L
s
.56346 .18791 m
.7148 .32416 L
s
.57629 .32416 m
.5763 .18137 L
s
.5763 .18137 m
.82366 .32416 L
s
.58767 .32416 m
.58768 .17399 L
s
.58768 .17399 m
1 .30794 L
s
.59744 .32416 m
.59745 .16609 L
s
.59745 .16609 m
1 .20837 L
s
.60554 .32416 m
.60555 .15801 L
s
.60555 .15801 m
1 .11653 L
s
.61195 .32416 m
.61196 .1501 L
s
.61196 .1501 m
1 .024 L
s
.61676 .32416 m
.61677 .14272 L
s
.61677 .14272 m
.86394 0 L
s
.6201 .32416 m
.62011 .13619 L
s
.62011 .13619 m
.77135 0 L
s
.62219 .32416 m
.6222 .13078 L
s
.6222 .13078 m
.71721 0 L
s
.62329 .32416 m
.6233 .12674 L
s
.6233 .12674 m
.67972 0 L
s
.6237 .32416 m
.62371 .12425 L
s
.62371 .12425 m
.65011 0 L
s
.62376 .32416 m
.62377 .1234 L
s
0 0 1 r
.01 w
.38095 .1234 m
.38095 .1234 L
.38095 .12341 L
.38095 .12342 L
.38095 .12344 L
.38095 .12348 L
.38096 .12354 L
.38096 .12362 L
.38097 .12373 L
.381 .12399 L
.38105 .12434 L
.38112 .12473 L
.38137 .12586 L
.38182 .12734 L
.38242 .12895 L
.38463 .13337 L
.38769 .13786 L
.39235 .14309 L
.39811 .14804 L
.40561 .15291 L
.40976 .15504 L
.41456 .15708 L
.41893 .1586 L
.42394 .15998 L
.42636 .16051 L
.42899 .161 L
.43128 .16135 L
.43381 .16165 L
.43529 .16178 L
.43667 .16189 L
.43799 .16196 L
.43866 .16199 L
.4394 .16201 L
.44006 .16203 L
.44067 .16204 L
.44134 .16205 L
.44167 .16205 L
.44204 .16205 L
.44266 .16204 L
.44332 .16203 L
.44395 .16201 L
.44452 .16199 L
.44569 .16194 L
.44698 .16186 L
.44958 .16164 L
.45197 .16135 L
.45419 .16102 L
.45911 .16004 L
.46422 .15865 L
.46883 .15705 L
Mistroke
.47359 .15502 L
.48141 .15068 L
.48789 .14589 L
.49338 .14056 L
.49748 .13528 L
.50004 .13085 L
.50088 .12898 L
.50152 .12727 L
.50192 .12592 L
.50216 .12492 L
.50229 .12412 L
.50233 .12381 L
.50234 .12369 L
.50235 .12359 L
.50235 .12352 L
.50236 .12349 L
.50236 .12346 L
.50236 .12344 L
.50236 .12343 L
.50236 .12342 L
.50236 .12341 L
.50236 .1234 L
.50236 .1234 L
.50236 .12341 L
.50236 .12342 L
.50236 .12343 L
.50236 .12345 L
.50236 .1235 L
.50237 .12357 L
.50237 .12367 L
.5024 .1239 L
.50244 .12426 L
.50251 .12467 L
.50277 .12584 L
.5032 .12726 L
.50465 .13079 L
.50692 .1348 L
.51027 .13933 L
.51531 .14456 L
.52132 .14935 L
.52912 .15403 L
.53772 .15773 L
.54238 .15921 L
.54508 .15992 L
.54769 .1605 L
.55011 .16095 L
.55277 .16136 L
.55521 .16165 L
.55747 .16185 L
.55874 .16193 L
Mistroke
.55993 .16199 L
.56062 .16201 L
.56124 .16203 L
.56159 .16203 L
.56196 .16204 L
.56232 .16205 L
.56265 .16205 L
.56299 .16205 L
.56337 .16205 L
.56372 .16205 L
.56404 .16204 L
.56466 .16203 L
.56531 .16202 L
.56601 .16199 L
.56666 .16197 L
.56814 .16188 L
.56946 .16178 L
.57067 .16167 L
.57341 .16135 L
.57581 .16099 L
.57832 .16052 L
.58259 .15953 L
.58715 .15817 L
.59561 .15473 L
.60362 .15016 L
.61006 .14524 L
.61539 .13987 L
.61905 .13504 L
.62137 .13101 L
.62228 .129 L
.62291 .12734 L
.6233 .12606 L
.62356 .12496 L
.62368 .12429 L
.62372 .12401 L
.62375 .12376 L
.62376 .12359 L
.62377 .12353 L
.62377 .12349 L
.62377 .12345 L
.62377 .12343 L
.62377 .12342 L
.62377 .12341 L
.62377 .12341 L
.62377 .1234 L
.62377 .1234 L
Mfstroke
0 0 0 r
.5 Mabswid
.38095 .1234 m
.38095 .1234 L
.38095 .12341 L
.38095 .12341 L
.38095 .12342 L
.38095 .12344 L
.38095 .12347 L
.38095 .12351 L
.38096 .12357 L
.38096 .1237 L
.38098 .12387 L
.38099 .12407 L
.38106 .12465 L
.38117 .12542 L
.38133 .12629 L
.38191 .12876 L
.38275 .13147 L
.38409 .13498 L
.38826 .14325 L
.39456 .1525 L
.40398 .163 L
.41578 .17306 L
.42957 .18203 L
.4374 .18611 L
.44657 .19014 L
.4559 .19348 L
.46477 .19603 L
.47426 .19811 L
.47958 .199 L
.48468 .19968 L
.48956 .20016 L
.49203 .20035 L
.49476 .20051 L
.49709 .2006 L
.4984 .20064 L
.49902 .20066 L
.4996 .20067 L
.50016 .20068 L
.50066 .20068 L
.50122 .20069 L
.50182 .20069 L
.50239 .20069 L
.50302 .20069 L
.50361 .20069 L
.50415 .20068 L
.50479 .20067 L
.50539 .20066 L
.50653 .20064 L
.50776 .2006 L
.50909 .20055 L
Mistroke
.51148 .20042 L
.5137 .20028 L
.51875 .19982 L
.5241 .19915 L
.52922 .19833 L
.53469 .19726 L
.54437 .19484 L
.5536 .19186 L
.56287 .18817 L
.57815 .18028 L
.59208 .17064 L
.60296 .16078 L
.61174 .15041 L
.61762 .14122 L
.62115 .13373 L
.62228 .13054 L
.62307 .12775 L
.62331 .12671 L
.6235 .12575 L
.62361 .12504 L
.62369 .12441 L
.62374 .12397 L
.62376 .12367 L
.62377 .12355 L
.62377 .12347 L
.62377 .12344 L
.62377 .12343 L
.62377 .12342 L
.62377 .12341 L
.62377 .1234 L
.62377 .1234 L
.62377 .1234 L
Mfstroke
% End of Graphics
MathPictureEnd
\
\>"], "Graphics",
 ImageSize->{306.562, 152.375},
 ImageMargins->{{58, 0}, {0, 0}},
 ImageRegion->{{-2.01069, 2.86509}, {-0.308377, 
  1.17791}},ImageCache->GraphicsData["CompressedBitmap", "\<\
eJztXeluHMcRHnGXS4qiREmUROqm7sNSLEuycvhSfMmO4hhJkCD+E0AxDFhA
ghiK8yMIkLxGnsEwjCAI8nqb7aqeqp6Zr2Zrjt2lRRLgctjT3XV83dXV1cd+
8uzrL7/407Ovn3/+bOfJi2dfffn887/sfPjnF5OkwYEsO3A/y7J/7WTheTx5
jB9Hx5OfbDD5zcbxJ1sDaSsg7X/OfKg+RPdi+FwKSUP6H2Y6BNJGPTO3YTFX
SotsllKPOKl04fqgk+sL4ZNUuqK88pNZBvG67OQVyeTl9bzwyiWyJZAJKRcR
6MLwahOGIS+s5dKrw07WEUv/deZD9RFd0qu2hnVmc9iBTaRhL5tIw4juOWGY
cYdEUUFEwMswyudl+KwwzLYLSu9lGKHjZRgZDZNhaJthFes9s47yofoQ3TPh
k3S9yQyvSkIXDf8HpHk1jNg8LVydZDaRfUWjGiLQhWHUFBHD28LwFjOMmENC
IA0jQ4gYRoIhBSBetkFaZB3J53VvurCOdE10Sa/aJM4wm6i9dmHz313ZRNok
hs8xw118nC4Mo3aN6J4Shi8ww2j89g6+XoZRPu8gfQqkRdaRF+xlHblQXtaR
rhFdMmuk60vM8DFnQUTAyzDK52X4hDB8hRk+Lgle3xeR/w6kIb2ido3Y3BSu
rjGbJ0AmZBHmwTCy9ZsgLbJ+shHry2lafJO5m4GXXUX9JlNYaqLkYQ+cImWv
CFeK/23hDwvhMATLOdMpuzmnWApUDUpDLESOB0VhHJXVqRW1V69aEa1jwt1d
prAsCV61DvPPolq/q5HC65odFWZe5ZLaMpCBtQeuRtx5vYQNYeZeeDpAhd1T
faSDWmYryagGlIaoE9qUdIArU88MjbU2s0laZnLqdWOOCBPKXwwwrcsrVbs9
2lc4+DanCfD2DvwaRHglPA2lQWYJ40jZiADS3rd+7aE0VdFt0R7PFSCmyOFG
nFbMi82pKWgSvyi+vSGcxujGsdrsJn9LCnKRU5fuC8pTltjGJIOQrbKSKmoY
8lrrQ1LLdWGIB+bE20RzO0RAdfSNU0eoFrUR14SlOMncsrJ7umcNa6YwDlqX
hck4xdRZPZpCc7XYRHwjJb0MHZQSygYHS5Ogja0hTgvJ4c8wfH4aXozC0y/o
idYvBjmLealKfRoRuiSM7HAZDS0qtyZ5IrpSSx7pQUfsHSF/hctcsLit9A4m
lSUslbgZZDmf+bLOWIRJwunFes8LQ9fHefasEoLXMqHiJVGI8BJ+PxF26GmY
Ccc5O1OUo7zcEl5igqLEyllOsYm/GSsJsrFc1ApXnzSLc0LpDr9blgQUGR3x
W6iGn4d3B8PT0/C0jFFBGJ8WmtHTo0zDYq8dcRok/RSRHmQpAqrtban8PlNb
F/Ia5hkqtWLbozbwcXh7SJ6iqAV6k1/1preEwiMmuSEJGqgZqG4ByY/C28Py
hEQcSlWnpPofM73jkoAmoogopT0Rok9q5ESGZ1MIvskcnJQEHVGRfonuByHL
hjyNgLDau5XUO0xqWxJ4FoP7TOgZ4/dDhmPh6T0lVBFwqajCd5nMWUk4ZiFH
aZR9Mzz9NDytVGkkdUyePuDqL0jCEav6oVR6cKwGOX+7Gj5JJauY5BGh8BGX
0zHiUI3iyMS8HTKsAarEyVtKtYJarJzIPOWSVyVBDdMSkJfGvzeFMhFeD08/
IR0U6VHVajmV3oDFUE9P+ylSMhGlbkQdflmI/kiJDpUoU14qmtL3mee8k6c+
urZj7r+pqknK14XgijyRFVlT0mMpmQ7kVP/b3DzGuf/Kkysyf8xgCeMAz+SJ
TCP1/FV5eqBUl5lqkTJ7dj8UWTmkyzZ9iCUkWpThSHhakydKO4QlpLruiVzR
n1kNHzdUm4l3ksXRnfHN0WXiNJsjU7MuxGkoPKzNqVJLWvN1kfeWMBIT1Jvg
xLLCEtVnhOstYWZDmKHY3BEew5DWRfvqSpBaHvCr45KgUd+IQ1JB1fEL5mHy
RN7a0fB0XHij7rqRsY0Zc+ax/IDWuCkaiYPBWUnQWUU9Uzmd4+EfclrIap8U
9i5SWiYGKVdVddKRMLYmyvmQ39DUZr2OneIUIUqekXmn9k5P28IguTabOjyn
bdngjMh/yv+/Kgzmg6yYtixSpdn9SVHLpmB1Ig5dY2nFkQrV91lCoE7/y9Jx
tIItsUZbonpN207ossARiUGN0EtNeDKmlYVmOJ6Yq8n/l0UnF8PrIVAHF18p
4Ar7Vif28r4llnI8vhtSyGJRUzkvDC6RzlAt8+GSc0WjdV0sInX+M4FLjr0N
s6zIzHDOPHJaFidrO+N83CJbupXlg1c+WqU/2aK4HRd6cfiPeDwTcpEbQ24b
9d0YIACNdQFNYwTsAI2Y6j3T6Eoj1UbKeez9w44tpDjqkg1L47R5z/pnWWm5
lIdALZEKeWNUY+ISJezBgGt51hMo/oMejH7hCDhFGWhk/DUnkHezJtJ2bH8o
Xjmp7+9UdZb5o4HuNjcS/j/mV6ckQWP5XQWqxu2CLH/jHIX+07t8NPxS23nM
r9ZFPt3l0r9844ynSNlfiUiWO6d2U2sp32kR51HCOSVc6Uu+YbkEe+hZ9oLT
Cp1ZpzaO+KdbzCsi1R0RIM6VWngiJbR0llntel9xctlk6Nyxk1gkwEERsKXt
r+CTv0XmhOqfVP9HerJsSqWd5g6Pe4TgObA00eaiJajUW3ZJI7me8/9TrYrZ
ExM32CHmYxFzwynmoEZgV1rKeT6sfkFUg9D2LgBXb6+fAfwsPFF7HVWkLfb2
xJ1oJ2Ul8JIYnj+ItGU+/Quy9b2pungSR3z2s1juUW2vdMvtX/imBv57ER76
KDXFmw0+xTSSNfpjPwh/Bv6e3EwdLkSRo/6ZKMYsgVqqdzeaxyRQid9xwiuq
pZVUnHLB7ipTRw69JSX9VnRj2gezlk7+SdpC+Pc3/O9NUY9S77c5KeL2Tj5u
bJn+/lIUBfODNKLQZyNSR5HK/opf6aq+LpX1rq9K53CNJur1ZXkMP8sQd66N
f9xKep8IrIj+YuyOVlOIbpMgZ61CTYewwX6y2KYmvNCaXmLPGuwxw3s4+nTI
10R7cZGotB7WRyDG7Fl2b8OD4Yro9D3R6agmv3dLnFpfe4xy6LKy0Fdaumys
yxUklUhna9SvDdIohXRpyYD6O9o6MBUVcyTq1Dorq7W65uxdZinSrNgnr1E1
9jXbB7YqXZe2iFDcdDkflhJx7TNNFZr2LvFRWbr82T3bI61S1e/yK91PoJs2
Gijc7FQu62q4OSOpOS45Z3FRcCQGQd86Bj1bodj+urWp+1He4Ve6uUH3p81g
Fq3+jOvYRoa2YcUg9GXiWGcpqKlP12Y9Qz1N4c9Ikbj8qbt/khXi6do2ZLVn
OtMHmAZb+nVrBlUUFs95S0HYEZAochUX9x6mQFzag9+oTKaoPBxk0O12cXeU
bgDbkUwGHubQ0ShtoIpqqHu0aZSqXKPmkUn/KCmqWELra4qW2d1tjLCtqgNo
RyjFmPNBSbgimUrjVD/xIEP1sLjT6cgwaGhPKqJVSRMaTU/XVESbXSSLUKIC
cReKth3aLjODyWUdJih/s0CEc6AiudD2cu1wFdyNkalB3KyR3L0vEOkBhXv8
Si3cLZG6GqXpNY7HVSPRXi6YXRari8gDbTc5UIwivYinBOmZlsnK2wgTLtrb
4loj5jd6xlDaDECXcutMj+tKCGM6YYOGtw0Mi7jE7YgzDaabMV1jyGngb84A
qDrvCmXtPWJNJWhpR8fCvFutFnMXS/YCVpv5gGG26pp8M+D8HRqZY+81R72P
eiWvh4Y+dVDj4ow6urOJptdMXfsAErmeJqQQmBmBa8ZtO0GqCqVgGNWyw5nU
kelvLak+hmpiagaQWnsSjVHtQNfkHuHfe6dV8GhaQrXEY5k3Qb39YV0Zclzu
JgJrjC/j2O3gu0zU2Ixmm/avbUtAtLVNxBPDOlXtbUV0YElTv/qLggXfC8in
xHpnv4yoXFAoiWqJp9avFtU7s5hEc6A73JvSEVyfX+e9c0mnCJ2WL5UwnS2j
WuJlCJflld6YMM9FYbRq086vrdg8G0bXwttUJEtcd4/Ft0RXgaMDlVRLvHvj
Eqi3Hc5tF6xdg3mDGehssK6bQTfZ1WG5P50sMLo2RZGOF79cFFrtNne4HCyv
Od6DMJeMQacOrQi+EZ4GKnmy3KmXe7U010Z424sxjKPFer2x29mgbK+Cd9tL
VNmJweXzv+69A4oc78wJT/EY8DlAthXQps9gw9twa9EuhNffQGe2LwTd+kco
J6ur5Wun5+o3+/c5mZGGYV4dUKyNLc5fvwkIwWqMLY12n7Q1zArX4/BEtcQL
APWaHb1ezMIUt7FGW4D2cZTKB7mo09BTYOjGl3jhpF4fNH0bnLkLrxl20+cN
uxm4ZvvsUE8z1VjpHvy3hM5jfoPuYZoKoemNN7Ki5hzemzZrBL18tNy912JH
0mlQ11ucR61q062TnYfDPQ9ky4FQgYoY6i54vW+vvK0hraDLPjNvmN8z/kzX
iXebmbct1pv2mQSRpqKhmL3Br/RiAf3CH5amWLCU1sWwarDE1Mv3EUt62+XE
b6VPFofMPE+KVNyUi24ATuCcuYWtpHm3GxgTuoWi7HB40FLCXALAO6Bw3Per
9zJfkqdZbCXcR75/5Cv8VTYWKqYRbj0neUWeZt/RjQjyy494/xvC3Tet0NND
foWUQJn6213abVt4bdy9HVBe4L0NCR7tq9uZOasN4zU7U6+BQq9xJvVEZns8
oPWW8ZexCbisYO/3NSjAEXud++mNvLsK+zoTsvsaQIe9I/NpBQOBmkCPN74P
JOFmroi0XLGOfhrEdIdnr7cGpKf+L3AZFNK0ZcTTJqjyG5jgIo44eDXhR8f7
jYlNY+u9biIcpyGP3tuEF3P9Xrj8uIuxo3C/WbwMzQKBofYiNobbFoV5RuYb
+Nz2TR2Lbwd1g93Cjt6gCslpSNyHaA2sfjv/kzh7BHvSQu8nc1CFCGCeYc74
LEfLczt7pQWU3MXeez9SgB7Pii0A+QraPhfVAlzTKmMaj2r14m5subF3R7SO
hfaONhKbBngNJebHaIHGe0G749mtlxH0SlZzVGyLO6KtR2rj16xflYSZHOAx
4qFdjk0vFudmB6ix3fdq0420QncXVMPfEhk7OlBQH5ibTuVeQXqKnmrtuveA
j9mXI8KXQaberk1tjrBphBaLa8Mz1LNAE33p8B1hi08A9Hhwa/rsq5GNnoLr
9B7jBRXZiX4QrW/RbhzRsSxkgeOXK+yAV7o7oz2iOnf2nldqvc9wTog2O+qO
PLEeR1e0iUq/1CRiexFk0q+738d2l2K7AQrTZU/0Nh5yR1+z3e6IZT9HaF96
hM1AtRvXo6Cwfg0KnxuI15YUM7X7Pi3X/NY7yGqQxaXceYJqxhPmcij2GCh8
D6TxCbz+TlKWZOjiQDmwNd96Ea2vpS2O3Q+9ooOR94SbCNpZkKnd/fr9gYYa
8kLhK/HSbPTsDuQmqPs14ZpNb/LFfa0u8zed870JWsczyQrBA3mKt0ZsA5rt
vn5hOk5e7AZI5kXD5zpuOxen9RQofB+kxQsjtsCr+ZxQ3pOoJs6rA0sEzn2p
JiKIANdDy7sXQQdGra9wN2jsDlSRUVXLyzK3O71sD99dgF3K3xQbzLh20FwY
uv5rSlqbY5mqOMBG55ofgjQe8eOta8VXenWEY8A1plFdBtx9/Md1XatiT/kv
AXYGVPdQ3kbMkcfc6Jute8HcrzfvmfP5Ye5vr81Ov7fCHE1SX5e3TAxOd6fe
bmBGvvaR7oo0dofqYEYBpEelkjAodVYyzWv+5HJ4zLgssdr5C3b864IiimsN
uPf5E4r70q2odB9QvM0J3n8w0+ss9jEsbDKv0bUiQpdgxmucplx2MK+rSBpc
47Io4FBsbPao7YCS8dB6duD/VGh+jA==\
\>"],ImageRangeCache->{{{-616.375, 877.312}, {198.312, -27.0625}} -> {-1, -4, \
0, 0}}],

Cell[BoxData[
 TagBox[
  RowBox[{"\[SkeletonIndicator]", "Graphics", "\[SkeletonIndicator]"}],
  False,
  Editable->False]], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{640, 750},
WindowMargins->{{228, Automatic}, {Automatic, 84}},
FrontEndVersion->"9.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2013)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 36, 0, 92, "Title"],
Cell[CellGroupData[{
Cell[640, 26, 28, 0, 44, "Subsection"],
Cell[671, 28, 56, 3, 49, "Text"],
Cell[730, 33, 293, 9, 49, "Text"],
Cell[1026, 44, 311, 10, 49, "Text"],
Cell[1340, 56, 99, 2, 30, "Text"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1476, 63, 36, 0, 44, "Subsection"],
Cell[1515, 65, 88, 2, 28, "Input",
 InitializationCell->True]
}, Open  ]],
Cell[CellGroupData[{
Cell[1640, 72, 27, 0, 80, "Section"],
Cell[CellGroupData[{
Cell[1692, 76, 398, 12, 28, "Input"],
Cell[2093, 90, 4346, 137, 221, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6476, 232, 31, 0, 44, "Subsection"],
Cell[CellGroupData[{
Cell[6532, 236, 204, 6, 28, "Input"],
Cell[6739, 244, 1543, 49, 120, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[8331, 299, 124, 5, 70, "Subsection"],
Cell[CellGroupData[{
Cell[8480, 308, 123, 3, 70, "Input"],
Cell[8606, 313, 436, 15, 70, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[9091, 334, 124, 5, 70, "Subsection"],
Cell[CellGroupData[{
Cell[9240, 343, 123, 3, 70, "Input"],
Cell[9366, 348, 245, 8, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9648, 361, 425, 13, 70, "Input"],
Cell[10076, 376, 337, 12, 70, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10474, 395, 23, 0, 70, "Section"],
Cell[CellGroupData[{
Cell[10522, 399, 401, 12, 70, "Input"],
Cell[10926, 413, 6165, 347, 70, 4620, 320, "GraphicsData", "PostScript", \
"Graphics"],
Cell[17094, 762, 134, 4, 70, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17265, 771, 471, 15, 70, "Input"],
Cell[17739, 788, 302, 6, 70, "Message"],
Cell[18044, 796, 386, 7, 70, "Message"],
Cell[18433, 805, 386, 7, 70, "Message"],
Cell[18822, 814, 302, 6, 70, "Message"],
Cell[19127, 822, 386, 7, 70, "Message"],
Cell[19516, 831, 359, 6, 70, "Message"],
Cell[19878, 839, 371, 7, 70, "Message"],
Cell[20252, 848, 371, 7, 70, "Message"],
Cell[20626, 857, 12518, 561, 70, 6241, 456, "GraphicsData", "PostScript", \
"Graphics"],
Cell[33147, 1420, 134, 4, 70, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

